<?php
namespace Mafiatest;

use Mafiatest\FBIDepartmentRepository\InspectorFBI as PoliceWorker;
use Mafiatest\MafiaRepository\MafiaFactory as MFactory;
use Mafiatest\MafiaRepository\MafiaGuy as MafiaPerson;
use Mafiatest\surveillanceMachine\SurveillanceMafiaMachine as SurveillanceSystem;

include 'src/autoload.php';

/**
 *  @const PoliceWorker::MAFIA_SECRET number of mafia promotion
 *  @var $maxMafias Creates number of mafias
 *  @var $randMafia Select aleatory mafia to arrest and release from prision
 *
 */

$separator = "<br>------------------<br>";

$maxMafias = 11;
$randMafia = rand(0,$maxMafias);


$mafiaGuy = new MafiaPerson();
$mafia    = new MFactory($maxMafias, $mafiaGuy);
$police   = new PoliceWorker(new SurveillanceSystem($mafia->organization));



echo 'Original status';
echo $separator;

echo $police->useMachine()->listOriginalMafiaStatus();

echo $separator;

echo $police->arrestMafia($randMafia);

echo $separator;

echo $police->useMachine()->listMafias();

echo $separator;

echo $police->releaseMafia($randMafia);

echo $separator;

echo $police->useMachine()->listMafias();

echo $separator;

echo 'Check greater boos';

echo $separator;

echo $police->useMachine()->compareBoss(rand(0,$maxMafias),rand(0,$maxMafias));



