<?php
// autoload classes mapping from namespace to directory structure.
spl_autoload_register(function ($className) {

    // Fixing Namespace and Separator
    $Class = str_replace(__NAMESPACE__ . '\\', '/', lcfirst($className));

    // Correct DIRECTORY_SEPARATOR
    $Class = str_replace(array('\\'), DIRECTORY_SEPARATOR,
        pathinfo(__DIR__, 3) . "/../" . DIRECTORY_SEPARATOR . $Class . '.php');
    // Get file real path

    if (!is_readable($Class)) {
        // File not found
        return false;
    } else {
        require_once($Class);
        return true;
    }
});
