<?php
namespace Mafiatest\surveillanceMachine;

interface SurveillanceMafiaMachineInterface
{

    /**
     * @return mixed
     */
    public function listOriginalMafiaStatus();

    /**
     * @return mixed
     */
    public function listMafias();

    /**
     * @param $index
     * @return mixed
     */
    public function markAsJailed($index);

    /**
     * @param $index
     * @return mixed
     */
    public function markAsReleasedFromJail($index);

    /**
     * @param $boss1
     * @param $boss2
     * @return mixed
     */
    public function compareBoss($boss1, $boss2);

    /**
     * @param $list
     * @return mixed
     */
    public function updateList($list);

}