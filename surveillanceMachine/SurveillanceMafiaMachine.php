<?php
namespace Mafiatest\surveillanceMachine;

use Mafiatest\FBIDepartmentRepository\InspectorFBI;
use Mafiatest\MafiaRepository\MafiaGuy;

final class SurveillanceMafiaMachine implements SurveillanceMafiaMachineInterface
{
    /** @var  $criminalsList */
    protected $criminalsList;
    /** @var  $iterator */
    private $iterator;
    /** @var  $secret */
    private $secret;

    /**
     * SurveillanceMafiaMachine constructor.
     * @param $mafiaOrganization
     */
    public function __construct($mafiaOrganization)
    {
        $this->criminalsList = $mafiaOrganization;
        $this->secret = InspectorFBI::MAFIA_SECRET;
    }

    /**
     * @return null|string
     */
    public function listOriginalMafiaStatus()
    {
        $rapport = null;
        foreach ($this->criminalsList as $index => $mafiaGuyRapport) {

            $isBoss = $index != 0 ? (($index) %  $this->secret == 0 ? 'is Boss' : 'isStupid') : 'is SuperBoss';
            $color  = ($isBoss == 'is Boss' ? 'red' : ($isBoss == 'is SuperBoss' ? 'blue' : ''));
            $rapport .= $mafiaGuyRapport->name . " : " .
                "<font color='{$color}'>" . $isBoss . "</font> "
                . "<br>";
        }
        return $rapport;
    }

    /**
     * @return null|string
     */
    public function listMafias()
    {
        $rapport = null;
        foreach ($this->criminalsList as $index => $mafiaGuyRapport) {

            $color = ($mafiaGuyRapport->isBoss == 'is Boss' ? 'red' : ($mafiaGuyRapport->isBoss == 'is SuperBoss' ? 'blue' : ''));
            if ($mafiaGuyRapport->getCriminalStatus() !== "arrested") {
                $rapport .= $mafiaGuyRapport->name . " : " .
                    "<font color='{$color}'>" . $mafiaGuyRapport->isBoss . "</font> "
                    . $mafiaGuyRapport->getOrganizationIndex()
                    . "<br>";
            }
        }
        return $rapport;
    }

    /**
     * @param $index
     * @return bool
     */
    public function markAsJailed($index)
    {
        $this->criminalsList[$index]->setCriminalStatus(MafiaGuy::CRIMINAL_STATUS_ARRESTED);
        $this->criminalsList[$index]->setOrganizationIndex(0);
        $this->iterator = 0;

        $update_list = $this->remap();

        if (!$update_list) {
            return false;
        }
        return true;
    }

    public function remap()
    {
        return array_map(array($this, 'updateList'), $this->criminalsList);
    }

    /**
     * @param $index
     * @return bool
     */
    public function markAsReleasedFromJail($index)
    {
        $this->criminalsList[$index]->setCriminalStatus(MafiaGuy::CRIMINAL_STATUS_RELEASED);
        $this->criminalsList[$index]->setOrganizationIndex($index);
        $this->iterator = 0;

        $update_list = $this->remap();

        if (!$update_list) {
            return false;
        }
        return true;
    }

    /**
     * @param $boss1
     * @param $boss2
     * @return string
     */
    public function compareBoss($boss1, $boss2)
    {
        $compareBoss1 = $this->criminalsList[$boss1]->getOrganizationIndex();
        $compareBoss2 = $this->criminalsList[$boss2]->getOrganizationIndex();
        $isBoss       = null;

        if ($this->criminalsList[$boss1]->isBoss !== 'is Boss') {

            $isBoss .= $boss1 . '  is not a boss ';
        }
        if ($this->criminalsList[$boss2]->isBoss !== 'is Boss') {

            $isBoss .= $boss2 . '  is not a boss ';
        }

        if (!is_null($isBoss)) {

            return $isBoss;

        } else {

            return $compareBoss1 === $compareBoss2 ? " equals " : ($compareBoss1 > $compareBoss2 ? $boss1 : $boss2);
        }

    }

    public function updateList($list)
    {

        if ($this->criminalsList[$this->iterator] !== 0) {

            $iterator = $list->getCriminalStatus() === "Free" ? $this->iterator : $this->iterator--;

            $list->isBoss = $this->iterator != 0 ? (($iterator) %  $this->secret == 0 ? 'is Boss' : 'isStupid') : 'is SuperBoss';
        }
        $this->iterator++;
    }
}