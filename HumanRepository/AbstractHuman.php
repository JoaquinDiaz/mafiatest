<?php
namespace Mafiatest\HumanRepository;


abstract class AbstractHuman implements HumanInterface
{
    /** @var bool $isAlive */
    protected $isAlive = true;

    /**
     * @return mixed
     */
    abstract function isAlive();

    /**
     * @return mixed
     */
    abstract function isDead();

}