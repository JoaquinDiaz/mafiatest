<?php
namespace Mafiatest\FBIDepartmentRepository;

use Mafiatest\HumanRepository\AbstractHuman;
use Mafiatest\surveillanceMachine\SurveillanceMafiaMachine;


final class InspectorFBI extends AbstractHuman implements InspectorMachinesInterface,MafiaOperationsInterface
{
    /** @const MAFIA_SECRET */
    const MAFIA_SECRET = 3;
    /** @var  $machine */
    protected $machine;
    /** @var  $index */
    protected $index;

    /**
     * InspectorFBI constructor.
     * @param SurveillanceMafiaMachine $machine
     */
    public function __construct(SurveillanceMafiaMachine $machine)
    {
        $this->machine = $machine;
    }

    /**
     * Method to arrestMafia
     * @param $index
     * @return string
     */
    public function arrestMafia($index)
    {

        if (!$this->machine->markAsJailed($index)) {

            return "Mafia escaped!";
        }

        return "This Mafia num " . $index . " has been Jailed!";
    }

    /**
     * Method to releaseMafia
     * @param $index
     * @return string
     */
    public function releaseMafia($index)
    {
        if (!$this->machine->markAsReleasedFromJail($index)) {

            return "This Mafia num " . $index . " hasn't been Released From Jail!";
        }

        return "This Mafia num " . $index . " has been Released From Jail!";

    }

    /**
     * Accessor to SurveillanceMafiaMachine
     * @return SurveillanceMafiaMachine
     */
    public function useMachine()     //TODO move to a abstact
    {
        return $this->machine;
    }

    public function isAlive()
    {
        // TODO: Implement isAlive() method.
    }

    public function isDead()
    {
        // TODO: Implement isDead() method.
    }
}
