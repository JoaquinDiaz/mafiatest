<?php
namespace Mafiatest\FBIDepartmentRepository;

interface MafiaOperationsInterface
{

    /**
     * @param $index
     * @return mixed
     */
    public function arrestMafia($index);

    /**
     * @param $index
     * @return mixed
     */
    public function releaseMafia($index);

}