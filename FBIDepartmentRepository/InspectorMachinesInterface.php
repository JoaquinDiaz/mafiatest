<?php
namespace Mafiatest\FBIDepartmentRepository;

interface InspectorMachinesInterface
{

    /**
     * @return mixed
     */
    public function useMachine();
}
