<?php
namespace Mafiatest\MafiaRepository;

use Mafiatest\HumanRepository\AbstractHuman;


final class MafiaGuy extends AbstractHuman implements MafiaAuthoritiesRelationshipInterface
{
    const CRIMINAL_STATUS_ARRESTED = 'arrested';
    const CRIMINAL_STATUS_RELEASED = 'Free';

    /** @var  $name */
    public $name;
    /** @var  $criminalStatus */
    public $criminalStatus;
    /** @var  $status */
    private $status;
    /** @var  $organizationIndex */
    private $organizationIndex;

    public function getOrganizationIndex()
    {
        return $this->organizationIndex;
    }

    public function setOrganizationIndex($index)
    {
        $this->organizationIndex = $index;
    }

    public function getCriminalStatus()
    {
        return $this->status;
    }

    public function setCriminalStatus($status)
    {
        $this->status = $status;
    }

    public function leaveOrganization()
    {
        // TODO: Implement leaveOrganization() method.
    }

    public function reEnterOrganization()
    {
        // TODO: Implement reEnterOrganization() method.
    }

    public function isAlive()
    {
        // TODO: Implement isAlive() method.
    }

    public function isDead()
    {
        // TODO: Implement isDead() method.
    }
}