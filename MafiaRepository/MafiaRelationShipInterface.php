<?php
namespace Mafiatest\MafiaRepository;

interface MafiaRelationShipInterface
{

    /**
     * @return mixed
     */
    public function getOrganizationIndex();

    /**
     * @param $organizationIndex
     * @return mixed
     */
    public function setOrganizationIndex($organizationIndex);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);
}
