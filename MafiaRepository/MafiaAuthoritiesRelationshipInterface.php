<?php
namespace Mafiatest\MafiaRepository;

interface MafiaAuthoritiesRelationshipInterface {

    /**
     * @return mixed
     */
    public function leaveOrganization();

    /**
     * @return mixed
     */
    public function reEnterOrganization();

}