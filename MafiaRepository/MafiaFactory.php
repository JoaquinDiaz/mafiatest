<?php
namespace Mafiatest\MafiaRepository;

class MafiaFactory
{
    /** @var  $name */
    public $name;
    /** @var array $organization */
    public $organization = array();
    /** @var  $mafiaMemberName */
    protected $mafiaMemberName;
    /** @var  $newMafias */
    protected $newMafias;

    /**
     * MafiaFactory constructor.
     * @param $newQtyMafias
     * @param MafiaGuy $mafiaGuy
     */
    public function __construct($newQtyMafias, MafiaGuy $mafiaGuy)
    {

        for ($index = 0; $newQtyMafias >= $index; $index++) {

            $this->newMafias       = new $mafiaGuy($index);
            $this->newMafias->name = 'Mafia_' . $index;
            $this->newMafias->setOrganizationIndex($index);
            $this->newMafias->setCriminalStatus('Free');

            array_push($this->organization, $this->newMafias);
        }
        return $this->organization;
    }
}